import figures.{Circle, Rectangle, Triangle}

object Main extends App{
  override def main(args: Array[String]) {
    val figuresMap =
      Map("Triangle" -> Triangle(1, 2, 0.1),
          "Rectangle" -> Rectangle(5,3),
          "Circle" -> Circle(2))

    val figuresList = List(Triangle(2, 4, 0.2), Circle(7), Rectangle(4,1))

    //Perimeter
    figuresMap.foreach(figure => println(figure._2 + " perimeter = " + figure._2.perimeter))

    //Area
    figuresMap.foreach(figure => println(figure._2 + " area = " + figure._2.area))

    val newFiguresList = figuresList :+ Circle(14)


    newFiguresList.foreach(figure => println("List member " + figure + " perimeter = " + figure.perimeter))

    newFiguresList.foreach(figure => println("List member " + figure + " area = " + figure.area))
  }
}
