package figures


trait Figure {
  def perimeter = this match {
    case Triangle(leftSide, rightSide, angle) => 0.5 * leftSide * rightSide * math.sin(angle)
    case Circle(radius) => 2 * math.Pi * radius
    case Rectangle(length, width) => 2 * (length + width)
    case _ => throw new UnsupportedClassVersionError()
  }

  def area = this match {
    case Triangle(leftSide, rightSide, angle) => 0.5 * leftSide * rightSide * math.sin(angle)
    case Circle(radius) => math.Pi * radius * radius
    case Rectangle(length, width) => length * width
    case _ => throw new UnsupportedClassVersionError()
  }
}



